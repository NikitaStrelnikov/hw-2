const mungotinae = require('mongoose');

const User = mungotinae.model('User', {
  username: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
});

module.exports = {
  User,
};
