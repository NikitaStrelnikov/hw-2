const mungotinae = require('mongoose');

const Note = mungotinae.model('Notes', {
  userId: {
    type: mungotinae.Schema.Types.ObjectId,
    required: true,
  },
  text: {
    type: String,
    required: true,
  },
});

module.exports = {
  Note,
};
