const {Note} = require('../models/noteModel');
let offset = 0;
let limit = 0;

const getNotesByUserId = async (userId, req) => {
  offset = req.query.offset;
  limit = req.query.limit;
  const notes = await Note.find({userId}).slice(offset, offset + limit);
  return notes;
};

const getNoteByIdForUser = async (bookId, userId) => {
  const note = await Note.findOne({_id: bookId, userId});
  return note;
};

const addNoteToUser = async (userId, noteInfo) => {
  const note = new Note({...noteInfo, userId});
  await note.save();
};

const updateNoteByIdForUser = async (noteId, userId, data) => {
  await Note.findOneAndUpdate({_id: noteId, userId}, {$set: data});
};

const deleteNoteByIdForUser = async (noteId, userId) => {
  await Note.findOneAndRemove({_id: noteId, userId});
};

module.exports = {
  getNotesByUserId,
  getNoteByIdForUser,
  addNoteToUser,
  updateNoteByIdForUser,
  deleteNoteByIdForUser,
};
