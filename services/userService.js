const {User} = require('../models/userModel');

const getUserInfoById = async (userId) => {
  const user = await User.findOne({_id: userId});
  return user;
};

const updateUserPasswordById = async (userId, password) => {
  await User.findByIdAndUpdate(userId, {password});
};

const deleteUserById = async (userId) => {
  await User.findByIdAndDelete(userId);
};

module.exports = {
  getUserInfoById,
  updateUserPasswordById,
  deleteUserById,
};
