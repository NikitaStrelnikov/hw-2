const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

require('dotenv').config();

const {User} = require('../models/userModel');

const registration = async ({username, password}) => {
  const user = new User({
    username,
    password: await bcrypt.hash(password, 10),
  });
  await user.save();
};

const signIn = async ({username, password}) => {
  const user = await User.findOne({username});

  if (!user) {
    throw new Error('Invalid email or password');
  }

  if (!(await bcrypt.compare(password, user.password))) {
    throw new Error('Invalid email or password');
  }

  const token = jwt.sign({
    _id: user._id,
    username: user.username,
  }, process.env.SECRET);
  return token;
};

module.exports = {
  registration,
  signIn,
};
