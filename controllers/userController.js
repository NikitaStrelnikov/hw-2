const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();

const {
  getUserInfoById,
  updateUserPasswordById,
  deleteUserById,
} = require('../services/userService');

const {
  asyncWrapper,
} = require('../utils/apiUtils');

const {InvalidRequestError} = require('../utils/errorUtils');

router.get('/me', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  console.log(req.user);

  const user = await getUserInfoById(userId);

  if (!user) {
    throw new InvalidRequestError('No user with such id found');
  }

  res.json({user});
}));

router.patch('/me', asyncWrapper(async (req, res) => {
  const {userId} = req.user;

  await updateUserPasswordById(userId, req.body.newPassword);
  res.json({
    message: 'Success',
  });
}));

router.delete('/me', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  await deleteUserById(userId);

  res.json({
    message: 'Success',
  });
}));

module.exports = {
  userRouter: router,
};
