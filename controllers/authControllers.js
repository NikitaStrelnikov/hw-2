const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();

const {
  registration,
  signIn,
} = require('../services/authService');

const {
  registrationValidator,
} = require('../middlewares/validationMiddleware');

const {
  asyncWrapper,
} = require('../utils/apiUtils');

const {InvalidRequestError} = require('../utils/errorUtils');

router.post('/register', registrationValidator,
    asyncWrapper(async (req, res) => {
      const {
        username,
        password,
      } = req.body;

      if (!username || !password) {
        throw new InvalidRequestError('Invalid username or password');
      }

      await registration({
        username,
        password,
      });

      res.json({
        message: 'Success',
      });
    }));

router.post('/login', asyncWrapper(async (req, res) => {
  const {
    username,
    password,
  } = req.body;

  if (!username || !password) {
    throw new InvalidRequestError('Invalid username or password');
  }

  const token = await signIn({
    username,
    password,
  });

  res.json({
    message: 'Success',
    jwt_token: token,
  });
}));

module.exports = {
  authRouter: router,
};
