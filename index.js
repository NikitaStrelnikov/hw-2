// installing dependencies
const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const app = express();

require('dotenv').config();

const {notesRouter} = require('./controllers/notesController');
const {authRouter} = require('./controllers/authControllers');
const {userRouter} = require('./controllers/userController');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {CustomError} = require('./utils/errorUtils');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/notes', [authMiddleware], notesRouter);
app.use('/api/users', [authMiddleware], userRouter);

app.use((err, req, res, next) => {
  if (err instanceof CustomError) {
    return res.status(err.status).json({
      message: err.message,
    });
  }
  res.status(500).json({
    message: err.message,
  });
});

const initiate = async () => {
  try {
    await mongoose.connect(`mongodb+srv://anonymus-admin:superDuperSimplePassword1@notepadx.v2sdn.mongodb.net/test`, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
        .then(() => console.log('Connected succesfully'))
        .catch((err) => console.error(err));

    app.listen(process.env.PORT, process.env.HOST);
  } catch (error) {
    console.error(`Error on server startup: ${error.message}`);
  }
};

initiate();
