/**
 * The root custom error-class
 */
class CustomError extends Error {
/**
 * Adds two numbers together.
 * @param {string} message The message text
*/
  constructor(message) {
    super(message);
    this.status = 500;
  }
}

/**
 * Invalid request error-class
 */
class InvalidRequestError extends CustomError {
/**
 * Adds two numbers together.
 * @param {string} message The message text
*/
  constructor(message = 'Invalid request') {
    super(message);
    this.status = 400;
  }
}

module.exports = {
  CustomError,
  InvalidRequestError,
};
