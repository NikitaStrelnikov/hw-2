const jwt = require('jsonwebtoken');

require('dotenv').config();

const authMiddleware = (req, res, next) => {
  const {
    authorization,
  } = req.headers;
  const message = 'Please, provide "authorization" header';

  if (!authorization) {
    return res.status(401).json({message});
  }

  const [, token] = authorization.split(' ');

  if (!token) {
    return res.status(401).json({message: 'Please, include token to request'});
  }
  try {
    const tokenPayload = jwt.verify(token, process.env.SECRET);
    req.user = {
      userId: tokenPayload._id,
      username: tokenPayload.username,
      password: tokenPayload.password,
    };
    next();
  } catch (err) {
    res.status(401).json({message: err.message});
  }
};

module.exports = {
  authMiddleware,
};
